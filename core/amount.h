#pragma once
#include <algorithm>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

namespace jobcoin::core {
class Amount final
{
private:
  int integral;
  int fractional;

public:
  Amount()
    : integral(0)
    , fractional(0)
  {}

  Amount(const std::string& input)
  {
    int indexOfDot = input.find_first_of('.');
    integral = stoi(input.substr(0, indexOfDot));
    fractional = 0;
    if (indexOfDot < input.size()) {
      fractional = stoi(input.substr(indexOfDot + 1));
    }
  }

  Amount(std::istream& is)
    : Amount([](auto& is) {
      std::string input;
      is >> input;
      return input;
    }(is))
  {}

  bool operator==(const core::Amount& other) const
  {
    return this->integral == other.integral &&
           this->fractional == other.fractional;
  }

  bool operator<(const core::Amount& other) const
  {
    return this->integral < other.integral ||
           (this->integral == other.integral &&
            this->fractional < other.fractional);
  }

  bool operator<=(const core::Amount& other) const
  {
    return *this < other || *this == other;
  }

  core::Amount operator-=(const core::Amount& other)
  {
    this->integral -= other.integral;
    this->fractional -= other.fractional;
    return *this;
  }
  core::Amount operator+=(const core::Amount& other)
  {
    this->integral += other.integral;
    this->fractional += other.fractional;
    return *this;
  }

  core::Amount operator+(const core::Amount& other)
  {
    core::Amount amount;
    amount.integral = this->integral + other.integral;
    amount.fractional = this->fractional + other.fractional;
    return amount;
  }

  std::string to_string() const
  {
    return std::to_string(integral) + '.' + std::to_string(fractional);
  }

  friend std::ostream& operator<<(std::ostream& os, const core::Amount& amount);
};

std::ostream&
operator<<(std::ostream& os, const core::Amount& amount)
{
  os << amount.integral << "." << amount.fractional;
  return os;
}
}