#pragma once
#include <string>
namespace jobcoin::core {
class Address final
{
private:
  std::string address;

public:
  Address(const std::string& addr)
    : address(addr)
  {
    if (address.empty()) {
      throw std::invalid_argument("Address cannot be empty");
    }
  }

  Address(std::istream& is)
    : Address([](auto& is) {
      std::string input;
      is >> input;
      return input;
    }(is))
  {}

  bool operator<(const Address& other) const { return address < other.address; }

  friend std::ostream& operator<<(std::ostream& os,
                                  const core::Address& walletAddress);

  std::string to_string() const { return address; }
};

std::ostream&
operator<<(std::ostream& os, const core::Address& walletAddress)
{
  os << walletAddress.address;
  return os;
}
}