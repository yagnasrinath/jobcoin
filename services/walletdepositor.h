#pragma once

#include <cpprest/http_client.h>

namespace jobcoin::services {
using namespace utility;
using namespace web::http;
using namespace web::json;
using namespace web::http::client;
using namespace concurrency::streams;

namespace {
utility::string_t fromAddressKey(U("fromAddress"));
utility::string_t toAddressKey(U("toAddress"));
utility::string_t amountKey(U("amount"));
}

// TODO: This class can be made asynchronous and can be passed a callback that
// can schedule the next split or notify the user
template<typename Address = core::Address, typename Amount = core::Amount>
class WalletDepositor
{
private:
  web::http::client::http_client webClient;

public:
  WalletDepositor(
    const std::string url =
      "http://jobcoin.gemini.com/booth-recipient/api/transactions")
    : webClient(U(url))
  {}

  bool depositCoins(const Address& from,
                    const Address& to,
                    const Amount& amount)
  {
    web::json::value obj = web::json::value::object();
    obj[fromAddressKey] = web::json::value(from.to_string());
    obj[toAddressKey] = web::json::value(to.to_string());
    obj[amountKey] = web::json::value(amount.to_string());
    bool isSuccessful = false;
    webClient.request(methods::POST, "", obj)
      .then([&isSuccessful](http_response response) -> void {
        if (response.status_code() == 200) {
          isSuccessful = true;
        }
      })
      .wait();
    return isSuccessful;
  }

  ~WalletDepositor() {}

  void stop() {}
};

template<typename Address = core::Address, typename Amount = core::Amount>
WalletDepositor<Address>&
getWalletDepositor()
{
  static auto depositor = WalletDepositor<Address>();
  return depositor;
}
}
