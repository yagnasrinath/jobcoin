#pragma once
#include "../utils/time_utils.h"
#include "address.h"
#include "amount.h"
#include <iomanip>
#include <iostream>

namespace jobcoin::core {

template<typename Address = core::Address,
         typename Amount = core::Amount,
         typename TimePoint = int64_t>
class Split
{
private:
  Address toAddress;
  Amount amount;
  // This value can be set based on factors like level of anonymity user
  // requested This also depends on number of transactions that are happening
  // from house's wallet address during the period. Also, there could be machine
  // learning models which can be optimized to issue transactions when the
  // demand is low that way, we can save on fees that need to be paid to make
  // the transaction.
  TimePoint depositAfter;
  // Not used for now. User can provide a deadline
  // TimePoint depositBefore;
public:
  Split(std::istream& is)
    : toAddress([](auto& is) {
      std::cout << "Please enter the new wallet address: "
                << "\n";
      return Address(is);
    }(is))
    , amount([](auto& is) {
      // TODO: Allow percentages
      std::cout << "Please enter full or partial amount to be sent over to new "
                   "address: "
                << "\n";
      return Amount(is);
    }(is))
    , depositAfter([](auto& is) {
      // Ideally we would have taken input from user in some
      // date time format and converted it to epoch time
      // based on UTC.
      std::cout << "Please enter the time in seconds(>120)"
                << "after which the deposit should be made"
                << "(Higher the number, Higher the anomity):";

      try {
        uint32_t input;
        is >> input;
        auto sec = jobcoin::utils::time::getSeconds(input);
        if (sec < 120) {
          throw std::invalid_argument(
            "Time should be greater than 120 seconds");
        }
        return sec;
      } catch (...) {
        std::cout << "Invalid Time Input" << std::endl;
        std::rethrow_exception(std::current_exception());
      }
    }(is))
  {}

  bool operator<(const Split& other) const
  {
    return this->depositAfter < other.depositAfter;
  }

  const Amount& getAmount() const { return amount; }

  const TimePoint& getDepositAfter() const { return depositAfter; }

  const Address& getToAddress() const { return toAddress; }

  friend std::ostream& operator<<(std::ostream& os, Split& split)
  {
    auto t_c = jobcoin::utils::time::getTime(split.depositAfter);
    os << "toAddress: " << split.toAddress << ", amount: " << split.amount
       << ", deposit After: "
       << std::put_time(std::localtime(&t_c), "%F %T.\n");
    return os;
  }
};

}