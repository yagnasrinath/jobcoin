#pragma once

#include "../core/transaction.h"
#include "walletdepositor.h"
#include <condition_variable>
#include <shared_mutex>
#include <thread>
namespace jobcoin::services {
namespace {
const jobcoin::core::Address houseAddress("house");
}
template<typename WalletDepositor, typename Transaction = core::Transaction<>>
class SplitDepositor
{
  struct TxNotifier
  {
    Transaction transaction;
    std::function<void()> callback;
    bool operator<(const TxNotifier& other) const
    {
      return this->transaction < other.transaction;
    }
  };

private:
  WalletDepositor& walletDepositor;
  std::priority_queue<TxNotifier, std::vector<TxNotifier>> txNotifiers;
  std::atomic<bool> done;
  std::mutex m;
  std::condition_variable cv;
  std::thread processorThread;

public:
  SplitDepositor(WalletDepositor& wd)
    : walletDepositor(wd)
    , done(false)
    , processorThread([this]() {
      while (!done) {
        std::unique_lock lock(m);
        auto sleepFor = 3; // 3 seconds from now;

        if (!txNotifiers.empty()) {
          auto sleepFor = txNotifiers.top()
                            .transaction.getNextPendingSplit()
                            .getDepositAfter() -
                          jobcoin::utils::time::getSeconds(0);
          if (sleepFor < 0) {
            // split could have been already ready
            sleepFor = 0;
          }
        }

        cv.wait_for(lock, std::chrono::seconds(sleepFor));
        auto isReady =
          (!txNotifiers.empty()) &&
          (txNotifiers.top()
             .transaction.getNextPendingSplit()
             .getDepositAfter() <= jobcoin::utils::time::getSeconds(0));
        if (!isReady) {
          continue;
        }
        auto tx = txNotifiers.top().transaction;
        auto cb = txNotifiers.top().callback;
        txNotifiers.pop();
        auto& split = tx.getNextPendingSplit();
        std::cout << "Depositing coins " << split.getAmount()
                  << " from house to " << split.getToAddress() << std::endl;
        walletDepositor.depositCoins(
          houseAddress, split.getToAddress(), split.getAmount());
        tx.markSplitAsDone(split);
        std::cout << "Deposited coins " << split.getAmount()
                  << " from house to " << split.getToAddress() << std::endl;
        if (tx.isDone()) {
          cb();
        } else {
          txNotifiers.push({ tx, cb });
        }
      }
    })
  {}

  void handleTransaction(Transaction&& transaction,
                         std::function<void()> onComplete)
  {
    std::unique_lock lock(m);
    auto wakeUp = false;
    if (txNotifiers.empty() ||
        txNotifiers.top().transaction.getNextPendingSplit().getDepositAfter() >
          transaction.getNextPendingSplit().getDepositAfter()) {
      wakeUp = true;
    }
    txNotifiers.push({ transaction, onComplete });
    if (wakeUp) {
      lock.unlock();
      cv.notify_one();
    }
  }

  void stop()
  {
    done = true;
  }

  ~SplitDepositor() {
      processorThread.join();
  }
  
};

template<typename WalletDepositor, typename Transaction = core::Transaction<>>
auto&
getSplitDepositor(WalletDepositor& wd)
{
  static auto depositor = SplitDepositor(wd);
  return depositor;
}
}
