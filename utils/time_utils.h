#pragma once
#include <chrono>
#include <ctime>

namespace jobcoin::utils::time {
using Clock = std::chrono::system_clock;
using TimePoint = std::chrono::time_point<Clock>;
int64_t
getSeconds(const uint32_t offset)
{

  const Clock::duration since_epoch = Clock::now().time_since_epoch();
  return std::chrono::duration_cast<std::chrono::seconds>(since_epoch).count() +
         offset;
}

std::time_t
getTime(const int64_t time)
{
  const Clock::duration since_epoch = Clock::now().time_since_epoch();
  auto d = std::chrono::seconds(
    time -
    std::chrono::duration_cast<std::chrono::seconds>(since_epoch).count());
  auto tp = TimePoint(d);
  return std::chrono::system_clock::to_time_t(tp);
}
}