#include "core/address.h"
#include "core/transaction.h"
#include "services/splitdepositor.h"
#include "services/walletdepositor.h"
#include "services/walletwatcher.h"
#include <iostream>

namespace {
const jobcoin::core::Address houseAddress("house");
}

void
waitUntilCoinsDeposited(const jobcoin::core::Address& address,
                        const jobcoin::core::Amount& requiredBalance)
{
  std::condition_variable cv;
  std::mutex m;

  auto callback = [&]() { cv.notify_one(); };

  auto predicate = [&](const auto& balance) {
    return requiredBalance <= balance;
  };

  std::unique_lock lock(m);
  jobcoin::services::getWalletWatcher().addWatcher(
    address, callback, predicate);
  std::cout << "Waiting for coins to appear into the provided wallet...\n";
  cv.wait(lock);
}

void
depositCoinsfromTempWalletToHouse(const jobcoin::core::Address& tempWallet,
                                  const jobcoin::core::Amount& amount)
{
  if (jobcoin::services::getWalletDepositor().depositCoins(
        tempWallet, houseAddress, amount)) {
    std::cout << "Transferred coins to house wallet. Initiating transfers "
                 "after the specified time in the splits...\n";
  } else {
    // TODO: retry a few times and give up. Possibly endpoint is down.
    std::cout
      << "Failed transferring coins to house wallet. Will retry later...\n";
    throw std::runtime_error("Failed to transfer");
  }
}

void
depositCoinsToProvidedWallets(jobcoin::core::Transaction<>&& transaction)
{
  auto& splitDepositor = jobcoin::services::getSplitDepositor(
    jobcoin::services::getWalletDepositor());
  std::condition_variable cv;
  std::mutex m;

  auto callback = [&]() { cv.notify_one(); };
  std::unique_lock lock(m);
  splitDepositor.handleTransaction(std::move(transaction), callback);
  cv.wait(lock);
}

int
main(int argc, char* argv[])
{
  auto transaction = jobcoin::core::Transaction(std::cin);
  const auto& toBeDepositedAmount = transaction.getToBeDepositedAmount();
  jobcoin::core::Address randomHouseWallet(
    std::string("tempRandHouseWallet") +
    std::to_string(
      std::chrono::system_clock::now().time_since_epoch().count()));
  std::cout << "Please transfer balance to the wallet with address: "
            << randomHouseWallet << "\n";
  waitUntilCoinsDeposited(randomHouseWallet, toBeDepositedAmount);
  std::cout << "Received coins. Initiating transfer to house wallet\n";
  depositCoinsfromTempWalletToHouse(randomHouseWallet, toBeDepositedAmount);
  waitUntilCoinsDeposited(houseAddress, toBeDepositedAmount);
  std::cout << "Received coins in house wallet\n";
  depositCoinsToProvidedWallets(std::move(transaction));
  std::cout << "Finished depositing the coins to provided wallets\n";

  auto& ww = jobcoin::services::getWalletWatcher();
  ww.stop();
  auto& wd = jobcoin::services::getWalletDepositor();
  wd.stop();
  auto& sd = jobcoin::services::getSplitDepositor(wd);
  sd.stop();
  return 0;
}