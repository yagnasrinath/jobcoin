#! /usr/bin/env bash

# This is for mac only, please refer to
# https://github.com/microsoft/cpprestsdk#getting-started
# for installation on other platforms
brew install cpprestsdk