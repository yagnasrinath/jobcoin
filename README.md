
**Functional Requirements**:



1. Provide an ability for the user to Initiate the transaction.
    1. User will provide an amount that needs to be mixed.
    2. User will provide a list of addresses that the mixed amount is going to be transferred to.
        1. This can be a specific amount for each address or percentage split.
        2. The user can also provide the time duration after which the transfer is initiated and maximum time before the transfer is done.
            1. This can be based on the amount of anonymity that the user desires. 
            2. This input can be at each address level or for the entire initial amount.
    3. Users will be charged a fixed amount per transaction.
    4. Users will be provided a unique address to which the amount to be mixed is transferred to.
2. User will complete the transaction.
3. System will start processing the transaction as soon as money is received in the provided address.
4. Once the amount is received depending on the specified timings, transactions are initiated.

**Non-functional Requirements**:



1. Responsive: Users are verified before they are allowed to access this service. This can take care of DDOS attacks. Users can initiate a lot of transactions without sending money into wallets. So, a limit on the number of transactions per some time period can limit the malicious users from attacking the service. Also, since users are verified by using their SSN or identifications according to local regulations, creating a large number of user accounts will also be limited.
2. Scalable: All services should be able to scale independently.
3. Consistency: Financial applications must maintain consistent state in case of failures.




![alt_text](./architecture.png "architecture")


**Components:**



1. **Transaction service**: This takes in user requests and all the configuration options like splits, times and level of anonymity, expected amount etc and persists them to the database.
2. **Public Block watcher**: This component can watch for blocks on the public block chain and update the database with transactions where the customer has transferred the specified amount. Also triggers a message on to the message queue, which starts further processing. Assuming there is 1 block per 10 mins and ~3000 transactions per block, we can find a balance between sql queries and the amount of entries in the clauses to get the intersection of transactions in the public block chain block and transactions in our database for which we haven’t received the expected amount. Once the expected amount is received, the public block chain service will put a message on a message queue that transfer could be initiated. 
3. **House Depositor Service**: Subscribes to messages written by Public Block watcher service and initiates transfer into house address and writes a message to a topic in message queue that funds transfer can be initiated. Will persist the state back to Database.
4. **Message Queue**: Persistent Message Queue like kafka can be used here to store messages that aid in further processing of transactions. 
5. **Fund Transfer Service**: This service is responsible for sending out the funds back to the user’s wallets based on the policies/configurations that the user created in transaction service. The service will read messages written to a topic by House Depositor Service and segregate the messages into various topics. Transactions can be divided into various buckets that can be processed periodically by this service. For example, each day can be divided into 48 topics, depending on the input policy/configuration and the time at which transaction costs of the network will be cheap, we can write the message back to certain topics. The service will read the current hour’s topic and initiate transfers to user’s wallets and update the database accordingly and write a message to the notifier topic. This is a batch processing system.
6. **User Notifier Service**: User will be notified that the transaction is complete via some push notification on mobile devices or email or other types of notifications that the transaction is complete.

**Other considerations**:

**Wallet Address Generator**: This is a classic use case for **bloom filter**. Any new address generated is checked in the bloom filter for non-existence. If it does not exist a new address is generated and added back to the filter. Although technically this can be achieved by monotonically increasing numbers too, it usually limits the address space. Address space from integers is 10^(Max number of wallet characters), whereas with alphabets and digits, we can get from 36 to 62, depending on case sensitivity. It is a tradeoff between the size used to represent the wallet address vs wallet address existence check.

**Anonymity:** Users select the level of anonymity and based on that application can do the deposits when there are a lot of transactions happening on the blockchain. But the cost might be higher. Otherwise users can also provide the time window in which their coins are deposited into wallets and in this case the fee can be optimized to as little as possible but trading off a small amount of anonymity. Also, there could be machine learning models that can predict the times at which the transactions can be scheduled given a time window or for highest anonymity.

Database usage in this case is write heavy and should be optimized accordingly.

All the components should be able to deal with failures and scale independently of other components. 

Some of the services can be merged together, although having a logical separation will help evolve them independently.

Ideally for a scalable Fund Transfer Service, there needs to be some implementation of time based queues something like netflix has created. See reference 1.

**Command Line Tool Implementation**:

The tool is implemented in C++. It has 4 main components:



1. **WalletWatcher**: It is synonymous to **Public Block Watcher, **in the distributed system based architecture. It watches for certain transactions/conditions in the public block chain, and notifies when the condition is met.
2. **WalletDepositor**: It is synonymous to **HouseDepositor, **in the distributed system based architecture. It deposits coins from randomly generated wallet provided to the user, to house wallet.
3. **SplitDepositor**: It is synonymous to **FundTransferService, **in the distributed system based architecture. It takes in user configuration as input and finds out appropriate times to transfer coins.
4. **Main**: This component takes user input, calls appropriate services to proceed with the transaction and finally notifies the user when the transaction is complete.

**The basic workflow starts with the user being provided a random wallet address to deposit the mixing amount to the wallet owned by the service, and then transfer the coins once received into the house wallet and then split the amount across various wallets according to user wallets after the time user has given as input. Here time is used as a substitute for anonymity, not considering any transaction volume and all.**




**Build Instructions:**


1. Install cpprest, openssl, clang/gcc toolchains, cmake
2. mkdir build
3. cd build
4. cmake .. -DOPENSSL_ROOT_DIR=<opensssl root directory>
5. make
6. There should be a binary named JobCoinMixer in the build directory.

**NOTE:** I have committed binaries built on mac as well just for the sake of showing that project that is compilable.

**Things to do:**



1. Add tests for each of the modules.
2. Although, it is best to document the module with lots of comments and invariants and preconditions and postconditions and design docs, due to lack of time, that was not addressed.
3. Use libraries like boost asio to make efficient implementations and usages of asynchronous operations like polling periodically, or scheduling split deposits etc.
4. Handle failure cases in each of the services.
5. Make build scripts system independent.
6. Make endpoints dynamic using service meshes or API gateways.


**References:**


1. https://netflixtechblog.com/distributed-delay-queues-based-on-dynomite-6b31eca37fbc
2. https://microsoft.github.io/cpprestsdk/index.html