#pragma once

#include <atomic>
#include <chrono>
#include <cpprest/http_client.h>
#include <map>
#include <shared_mutex>
#include <thread>

namespace jobcoin::services {
using namespace utility;
using namespace web::http;
using namespace web::json;
using namespace web::http::client;
using namespace concurrency::streams;

namespace {
const std::string balance{ "balance" };
}

template<typename Address = core::Address, typename Amount = core::Amount>
class WalletWatcher
{
public:
  using Predicate = std::function<bool(const Amount&)>;
  using Callback = std::function<void()>;

private:
  web::http::client::http_client webClient;
  std::thread poller;
  std::shared_mutex notifiersGuard;

  std::map<Address, std::pair<Callback, Predicate>> notifiers;
  std::atomic<bool> done;

public:
  WalletWatcher(uint16_t frequencySeconds = 5,
                const std::string url =
                  "http://jobcoin.gemini.com/booth-recipient/api/addresses")
    : done(false)
    , webClient(U(url))
    , poller([this, frequencySeconds]() {
      while (!done) {
        {
          std::shared_lock lock(notifiersGuard);
          std::this_thread::sleep_for(std::chrono::seconds(frequencySeconds));
          if (notifiers.empty()) {
            // TODO: busy loop fix
            continue;
          }
        }

        std::unique_lock lock(notifiersGuard);
        std::vector<pplx::task<void>> async_tasks;
        for (auto it = notifiers.begin(); it != notifiers.end(); ++it) {
          // TODO: Handle failures
          auto task =
            webClient.request(methods::GET, it->first.to_string())
              .then(
                [=](http_response response) -> pplx::task<web::json::value> {
                  return response.extract_json();
                })
              .then([=](web::json::value val) -> void {
                std::cerr << val << std::endl;
                auto currentBalance =
                  Amount(val.as_object()[balance].as_string());
                auto cbs = it->second;
                if (cbs.second(currentBalance)) {
                  cbs.first();
                  notifiers.erase(it->first);
                }
              });
          async_tasks.push_back(task);
        }

        for (const auto& async_task : async_tasks) {
          try {
            async_task.wait();
          } catch (const web::http::http_exception& ex) {
            std::cerr << "Failed to fetch the data" << std::endl;
          }
        }
      }
    })
  {}

  ~WalletWatcher() { poller.join(); }

  void addWatcher(const Address& addr, Callback cb, Predicate pred)
  {
    std::unique_lock lock(notifiersGuard);
    notifiers.emplace(addr, std::make_pair(cb, pred));
  }

  void stop() { done = true; }
};

template<typename Address = core::Address, typename Amount = core::Amount>
WalletWatcher<Address>&
getWalletWatcher()
{
  static auto watcher = WalletWatcher<Address>();
  return watcher;
}
}
