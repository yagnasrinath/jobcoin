#pragma once
#include "split.h"
#include <set>
#include <vector>

namespace jobcoin::core {
template<typename Split = core::Split<core::Address, core::Amount>,
         typename Address = core::Address,
         typename Amount = core::Amount>
class Transaction
{
private:
  Address fromAddress;
  Amount toBeDepositedAmount;
  std::set<Split> pendingSplits;
  std::vector<Split> completedSplits;

public:
  Transaction(std::istream& is)
    : fromAddress([](auto& is) {
      std::cout << "Enter the wallet address from which coins will be "
                   "transferred from: \n";
      return Address(is);
    }(is))
    , toBeDepositedAmount([](auto& is) {
      std::cout << "Enter the amount that will be transferred after the "
                   "transfer fee: \n";
      return Amount(is);
    }(is))
    , pendingSplits([](auto& is, const auto& totalAmount) {
      Amount splitSoFar;
      std::set<Split> splits;
      while (true) {
        std::cout << "Enter the split details: \n";
        try {
          auto split = Split(is);
          if (totalAmount < splitSoFar + split.getAmount()) {
            throw std::invalid_argument(
              "Total split amount more than deposited.");
          }
          splits.insert(split);
          splitSoFar += split.getAmount();
          if (totalAmount == splitSoFar) {
            break;
          }
        } catch (const std::exception& ex) {
          std::cerr << ex.what() << std::endl;
          std::cout
            << "Please enter again or press CTRL-C to abort the transaction";
        }
      }
      return splits;
    }(is, toBeDepositedAmount))
  {}

  bool operator<(const Transaction& other) const
  {
    assert(!this->isDone());
    assert(!other.isDone());
    return this->getNextPendingSplit() < other.getNextPendingSplit();
  }

  bool isDone() const { return pendingSplits.empty(); }

  const Split& getNextPendingSplit() const
  {
    if (isDone()) {
      throw std::logic_error(
        "Transaction is already done. No more splits to be transacted");
    }
    return *pendingSplits.begin();
  }

  void markSplitAsDone(const Split& split)
  {
    pendingSplits.erase(split);
    completedSplits.push_back(split);
  }

  const Amount& getToBeDepositedAmount() const { return toBeDepositedAmount; }
};

}